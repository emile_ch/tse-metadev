# Metadev
Projet Scolaire 3A Telecom Saint Etienne 

Plateforme Web de collaboration pour l'entreprise fictive METADEV.

A mettre en place: 
- un GED : moodle
- un espace de partage de document: nextcloud
- un gestionnaire de version de code: gitlab
- Une source unique d'autenficiation: LDAP


Pour simplifier l'utilisation des outils:
- Une interface web d'admin LDAP
- Une interface web de gestion de gestion des conteneurs: portainer
- Une page d'acceuil repertoriant les liens vers les services (embarquée dans un server httpd)


## Stack technique

- Docker
- Docker-compose
- Traefik en reverse proxy, portainer en Web UI d'administration de conteneurs. 
- Bases de données : mariadb
- DB et volumes isolés
- Montage des volumes dans l'OS hôte: un dossier *containers_volumes* sera créé dans le dossieren fonction du chemin défini par PATH_VOLUME dans .env


## Prérequis

- Avoir docker d'installé
- Avoir docker-compose d'installé
- Créer un network appelé "frontend" 
- De préférence utiliser linux ou MacOs comme hôte
- n'avoir aucun service actif sur les ports 80, 

## Pré-lancement :
- pour créer le network "frontend" `docker network create frontend`
- pour lancer traefik et portainer `docker-compose -f docker-utils.yml up -d` <br>
Ces deux services seront automatiquement redémarrés si Docker est lancé grâce aux options `restart: always` dans le fichier yml.

Vous pouvez maintenant accéder à traefik.localhost et portainer.localhost dans votre navigateur préféré. 
Si vous utilisez windows ça ne fonctionnera pas. Donc n'utilisez pas Windows **respectez vous**. Plus serieusement, Windows ne prend pas en compte "l'auto DNS" sur localhost actuellement. Vous devrez donc ajouter dans votre fichier hosts les adresses traefik.localhost et portainer.localhost sur l'ip 127.0.0.0
De la même manière, les autres adresses devront être ajoutées pour la suite du projet. 

## Lancement et configuration: 
### Démarrage des service
Démarrage des services : `docker-compose -p metadev -f metadev-compose.yml up -d` <br>
Arrêter les services : `docker-compose -p metadev -f metadev-compose.yml stop` <br>
Arrêter les services et effacer les conteneur : 
`docker-compose -p metadev -f metadev-compose.yml down` <br>

**Pour certains services, le temps d'instalation peut être long (gitlab et moodle par exemple).**

### Configuration des bases de données:
Les connections entre les bases de données et les services sont préconfigurées. 
Donc, il n'y a rien besoin de faire :smile: 
### Premier accès aux services:

- Nextcloud : lors de la première connection, vous crérez un user admin
- Moodle: user=user, password=bitnami

## Personnalisation, sauvegarde et mise à jour des services

### Personnalisation

Vous pouvez personnaliser le domaine de base ainsi que changer les mots de passe des db et de l'admin LDAP directement via le fichier .env


#### Sauvegarde :
- Vous pouvez sauvegarder régulièrement les données des services présents dans les volumes. L'espace occupé par cette sauvegarde et le temps de réétablissement seront bien inférieurs à un éventuels snapshot de tout l'hote. 

